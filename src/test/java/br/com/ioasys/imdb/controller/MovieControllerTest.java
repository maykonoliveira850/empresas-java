package br.com.ioasys.imdb.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
class MovieControllerTest {
  @Autowired private MockMvc mockMvc;

  @Test
  void listMoviesWithoutToken_ShouldReturns401() throws Exception {
    this.mockMvc
        .perform(MockMvcRequestBuilders.get("/movies"))
        .andExpect(MockMvcResultMatchers.status().isUnauthorized());
  }

  @Test
  @WithMockUser
  void givenIdThatDoesntExistsIt_ShouldReturns404() throws Exception {
    this.mockMvc
        .perform(MockMvcRequestBuilders.get("/movies/0"))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }
}
