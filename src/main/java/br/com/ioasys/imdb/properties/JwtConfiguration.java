package br.com.ioasys.imdb.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author maykon-oliveira
 * @since 6/8/21 4:51 PM
 */
@Getter
@Setter
@Primary
@Configuration
@ConfigurationProperties(prefix = "jwt.config")
public class JwtConfiguration {
  private String loginUrl = "/login/**";
  private Header header = new Header();
  private int expiration = 3600;
  private String privateKey = "SDGryrsdgzSERQ@$%4y5ygsd";

  @Getter
  @Setter
  public static class Header {
    private String name = "Authorization";
    private String prefix = "Bearer ";
  }
}
