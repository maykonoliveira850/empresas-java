package br.com.ioasys.imdb.security.filter;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import br.com.ioasys.imdb.security.ApplicationUser;
import br.com.ioasys.imdb.security.token.TokenCreator;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

/**
 * @author maykon-oliveira
 * @since 6/10/21 5:04 PM
 */
@AllArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
  private final AuthenticationManager manager;
  private final JwtConfiguration jwtConfiguration;
  private final TokenCreator tokenCreator;

  @SneakyThrows
  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    final var applicationUser =
        new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);

    if (applicationUser == null) {
      throw new UsernameNotFoundException("Unable to retrive the username or password");
    }

    final var authenticationToken =
        new UsernamePasswordAuthenticationToken(
            applicationUser.getUsername(), applicationUser.getPassword(), Collections.emptyList());

    authenticationToken.setDetails(applicationUser);

    return manager.authenticate(authenticationToken);
  }

  @Override
  protected void successfulAuthentication(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain chain,
      Authentication authResult) {
    final var signedJWT = tokenCreator.createSignedJWT(authResult);
    final var s = tokenCreator.encryptToken(signedJWT);
    response.addHeader(
        "Access-Control-Expose-Headers", "XSRF-TOKEN, " + jwtConfiguration.getHeader().getName());
    response.addHeader(
        jwtConfiguration.getHeader().getName(), jwtConfiguration.getHeader().getPrefix() + s);
  }
}
