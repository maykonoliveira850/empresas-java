package br.com.ioasys.imdb.security.config;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import br.com.ioasys.imdb.security.filter.JWTTokenAuthorizationFilter;
import br.com.ioasys.imdb.security.filter.JwtAuthenticationFilter;
import br.com.ioasys.imdb.security.token.TokenConverter;
import br.com.ioasys.imdb.security.token.TokenCreator;
import lombok.AllArgsConstructor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import javax.servlet.http.HttpServletResponse;

import static br.com.ioasys.imdb.utils.ControllerConstants.MOVIE_PATH;
import static br.com.ioasys.imdb.utils.ControllerConstants.USER_PATH;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * @author maykon-oliveira
 * @since 6/8/21 6:15 PM
 */
@EnableWebSecurity
@AllArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
  private final JwtConfiguration jwtConfiguration;
  private final TokenCreator tokenCreator;
  private final TokenConverter tokenConverter;
  private final UserDetailsService userDetailsService;
  private final PasswordEncoder passwordEncoder;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.addFilter(
            new JwtAuthenticationFilter(authenticationManager(), jwtConfiguration, tokenCreator))
        .addFilterAfter(
            new JWTTokenAuthorizationFilter(jwtConfiguration, tokenConverter),
            UsernamePasswordAuthenticationFilter.class);

    http.csrf()
        .disable()
        .cors()
        .configurationSource(r -> new CorsConfiguration().applyPermitDefaultValues())
        .and()
        .sessionManagement()
        .sessionCreationPolicy(STATELESS)
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(
            (req, res, e) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED))
        .and()
        .authorizeRequests()
        .antMatchers(
            GET,
            "/**/swagger-ui.html",
            "/**/swagger-resources/**",
            "/**/webjars/springfox-swagger-ui/**",
            "/**/v2/api-docs/**")
        .permitAll()
        .antMatchers(POST, USER_PATH, jwtConfiguration.getLoginUrl())
        .permitAll()
        .antMatchers(POST, "/users/admin", MOVIE_PATH)
        .hasRole("ADMIN")
        .antMatchers(USER_PATH)
        .hasRole("ADMIN")
        .anyRequest()
        .authenticated();
  }
}
