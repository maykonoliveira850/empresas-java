package br.com.ioasys.imdb.security.token;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.SignedJWT;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.nio.file.AccessDeniedException;

/**
 * @author maykon-oliveira
 * @since 6/10/21 5:13 PM
 */
@Component
@AllArgsConstructor
public class TokenConverter {
  private final JwtConfiguration jwtConfiguration;

  @SneakyThrows
  public String decryptToken(String encrypterToken) {
    final var jweObject = JWEObject.parse(encrypterToken);

    final var decrypter = new DirectDecrypter(jwtConfiguration.getPrivateKey().getBytes());

    jweObject.decrypt(decrypter);

    return jweObject.getPayload().toSignedJWT().serialize();
  }

  @SneakyThrows
  public void validateTokenSignature(String signedToken) {
    final var signedJWT = SignedJWT.parse(signedToken);

    final var rsaKey = RSAKey.parse(signedJWT.getHeader().getJWK().toJSONString());

    if (!signedJWT.verify(new RSASSAVerifier(rsaKey))) {
      throw new AccessDeniedException("");
    }
  }
}
