package br.com.ioasys.imdb.security;

import br.com.ioasys.imdb.entity.Role;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import static java.util.Collections.singleton;

/**
 * @author maykon-oliveira
 * @since 6/10/21 5:20 PM
 */
public class SecurityContextUtil {
  private SecurityContextUtil() {}

  public static void setSecurityContext(SignedJWT signedJWT) {
    try {
      var claimsSet = signedJWT.getJWTClaimsSet();
      String username = claimsSet.getSubject();
      if (username == null) throw new JOSEException("");
      var role = claimsSet.getStringClaim("role");
      var userId = claimsSet.getLongClaim("userId");

      var applicationUser =
          ApplicationUser.builder().email(username).id(userId).role(Role.valueOf(role)).build();

      final var authenticationToken =
          new UsernamePasswordAuthenticationToken(
              applicationUser, null, singleton(new SimpleGrantedAuthority("ROLE_" + role)));

      authenticationToken.setDetails(signedJWT.serialize());
      SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    } catch (Exception e) {
      e.printStackTrace();
      SecurityContextHolder.clearContext();
    }
  }
}
