package br.com.ioasys.imdb.security.filter;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import br.com.ioasys.imdb.security.token.TokenConverter;
import com.nimbusds.jwt.SignedJWT;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static br.com.ioasys.imdb.security.SecurityContextUtil.setSecurityContext;

/**
 * @author maykon-oliveira
 * @since 6/10/21 5:19 PM
 */
@AllArgsConstructor
public class JWTTokenAuthorizationFilter extends OncePerRequestFilter {
  protected final JwtConfiguration jwtConfiguration;
  protected final TokenConverter tokenConverter;

  @Override
  protected void doFilterInternal(
      HttpServletRequest req, HttpServletResponse res, FilterChain chain)
      throws ServletException, IOException {
    var header = req.getHeader(jwtConfiguration.getHeader().getName());

    if (header == null || !header.startsWith(jwtConfiguration.getHeader().getPrefix())) {
      chain.doFilter(req, res);
      return;
    }

    final String token = header.replace(jwtConfiguration.getHeader().getPrefix(), "").trim();

    setSecurityContext(decryptValidating(token));

    chain.doFilter(req, res);
  }

  @SneakyThrows
  private SignedJWT decryptValidating(String encryptedToken) {
    final var decryptToken = tokenConverter.decryptToken(encryptedToken);

    tokenConverter.validateTokenSignature(decryptToken);
    return SignedJWT.parse(decryptToken);
  }
}
