package br.com.ioasys.imdb.security.token;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import br.com.ioasys.imdb.security.ApplicationUser;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.UUID;

/**
 * @author maykon-oliveira
 * @since 6/10/21 5:07 PM
 */
@Component
@AllArgsConstructor
public class TokenCreator {
  private final JwtConfiguration jwtConfiguration;

  @SneakyThrows
  public SignedJWT createSignedJWT(Authentication authentication) {
    final var principal = (ApplicationUser) authentication.getPrincipal();

    final var jwtClaimsSet = createJWTClaimsSet(principal);

    final var rsaKeys = generateKeyPair();

    final JWK jwk =
        new RSAKey.Builder((RSAPublicKey) rsaKeys.getPublic())
            .keyID(UUID.randomUUID().toString())
            .build();

    final var signedJWT =
        new SignedJWT(
            new JWSHeader.Builder(JWSAlgorithm.RS256).jwk(jwk).type(JOSEObjectType.JWT).build(),
            jwtClaimsSet);

    final var rsassaSigner = new RSASSASigner(rsaKeys.getPrivate());
    signedJWT.sign(rsassaSigner);

    return signedJWT;
  }

  private JWTClaimsSet createJWTClaimsSet(ApplicationUser applicationUser) {
    return new JWTClaimsSet.Builder()
        .subject(applicationUser.getUsername())
        .claim("role", applicationUser.getRole().toString())
        .claim("userId", applicationUser.getId())
        .issuer("http://localhost:8080")
        .issueTime(new Date())
        .expirationTime(
            new Date(System.currentTimeMillis() + (jwtConfiguration.getExpiration() * 1000)))
        .build();
  }

  @SneakyThrows
  private KeyPair generateKeyPair() {
    final var generator = KeyPairGenerator.getInstance("RSA");
    generator.initialize(2048);

    return generator.genKeyPair();
  }

  @SneakyThrows
  public String encryptToken(SignedJWT signedJWT) {
    final var encrypter = new DirectEncrypter(jwtConfiguration.getPrivateKey().getBytes());
    final var jwt =
        new JWEObject(
            new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A192GCM)
                .contentType("JWT")
                .build(),
            new Payload(signedJWT));

    jwt.encrypt(encrypter);
    return jwt.serialize();
  }
}
