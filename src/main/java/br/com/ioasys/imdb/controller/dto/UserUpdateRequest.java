package br.com.ioasys.imdb.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * @author maykon-oliveira
 * @since 6/9/21 5:24 PM
 */
@Getter
@Setter
@Validated
public class UserUpdateRequest {
  @JsonIgnore private Long id;
  @NotBlank private String name;
}
