package br.com.ioasys.imdb.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author maykon-oliveira
 * @since 6/9/21 4:08 PM
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class EmailAlreadyTakenException extends RuntimeException {
  public EmailAlreadyTakenException() {
    super("Email already taken", null, false, false);
  }
}
