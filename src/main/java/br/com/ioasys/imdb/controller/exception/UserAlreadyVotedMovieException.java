package br.com.ioasys.imdb.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author maykon-oliveira
 * @since 6/9/21 9:42 PM
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserAlreadyVotedMovieException extends RuntimeException {
  public UserAlreadyVotedMovieException() {
    super("User already voted movie", null, false, false);
  }
}
