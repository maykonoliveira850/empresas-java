package br.com.ioasys.imdb.controller;

import br.com.ioasys.imdb.controller.dto.UserCreateRequest;
import br.com.ioasys.imdb.controller.dto.UserUpdateRequest;
import br.com.ioasys.imdb.controller.exception.EntityValidationException;
import br.com.ioasys.imdb.entity.User;
import br.com.ioasys.imdb.service.UserService;
import br.com.ioasys.imdb.service.specification.UserCriteria;
import br.com.ioasys.imdb.utils.ControllerConstants;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static br.com.ioasys.imdb.entity.Role.ADMIN;
import static br.com.ioasys.imdb.entity.Role.USER;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

/**
 * @author maykon-oliveira
 * @since 6/9/21 3:55 PM
 */
@RestController
@AllArgsConstructor
@RequestMapping(ControllerConstants.USER_PATH)
public class UserController {
  private final ModelMapper mapper;
  private final UserService userService;

  @GetMapping
  public Page<User> findAll(
      @SortDefault.SortDefaults({@SortDefault(sort = "name", direction = Sort.Direction.ASC)})
          Pageable pageable) {
    final var criteria = new UserCriteria();
    criteria.setRole(USER);
    criteria.setExcluded(false);
    return userService.findAll(criteria, pageable);
  }

  @PostMapping("/admin")
  @ResponseStatus(CREATED)
  public User createAminUser(
      @Valid @RequestBody UserCreateRequest createRequest, BindingResult result) {
    if (result.hasErrors()) {
      throw new EntityValidationException(result);
    }
    final var user = mapper.map(createRequest, User.class);
    user.setRole(ADMIN);
    return userService.save(user);
  }

  @PostMapping
  @ResponseStatus(CREATED)
  public User createUser(
      @Valid @RequestBody UserCreateRequest createRequest, BindingResult result) {
    if (result.hasErrors()) {
      throw new EntityValidationException(result);
    }
    final var user = mapper.map(createRequest, User.class);
    user.setRole(USER);
    return userService.save(user);
  }

  @DeleteMapping("{id}")
  @PreAuthorize("#id == authentication.principal.id")
  public ResponseEntity<Boolean> deleteUser(@PathVariable Long id) {
    userService.delete(id);
    return ResponseEntity.status(NO_CONTENT).build();
  }

  @PutMapping("{id}")
  @PreAuthorize("#id == authentication.principal.id")
  public User updateUser(
      @PathVariable Long id, @Valid @RequestBody UserUpdateRequest updateRequest) {
    updateRequest.setId(id);
    return userService.update(updateRequest);
  }
}
