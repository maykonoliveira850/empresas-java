package br.com.ioasys.imdb.controller.dto;

import br.com.ioasys.imdb.entity.MovieGender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author maykon-oliveira
 * @since 6/9/21 9:27 PM
 */
@Getter
@Setter
@Validated
public class MovieCreateRequest {
  @NotBlank private String name;
  @NotBlank private String director;
  @NotNull private MovieGender gender;
  @NotNull @NotEmpty private List<String> authors;
}
