package br.com.ioasys.imdb.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author maykon-oliveira
 * @since 6/9/21 5:20 PM
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
  public UserNotFoundException() {
    super("User not found", null, false, false);
  }
}
