package br.com.ioasys.imdb.controller.exception;

import org.springframework.validation.BindingResult;

/**
 * @author maykon-oliveira
 * @since 6/9/21 4:26 PM
 */
public class EntityValidationException extends RuntimeException {
  private final BindingResult result;

  public EntityValidationException(BindingResult result) {
    this.result = result;
  }

  public BindingResult getResult() {
    return result;
  }
}
