package br.com.ioasys.imdb.controller.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author maykon-oliveira
 * @since 6/9/21 3:48 PM
 */
@Getter
@Setter
@Validated
public class UserCreateRequest {
  @NotBlank @Email private String email;
  @NotBlank private String name;

  @NotBlank
  @Size(min = 6)
  private String password;
}
