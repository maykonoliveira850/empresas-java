package br.com.ioasys.imdb.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author maykon-oliveira
 * @since 6/9/21 3:16 PM
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MovieNotFoundException extends RuntimeException {
  public MovieNotFoundException() {
    super("Movie not found", null, false, false);
  }
}
