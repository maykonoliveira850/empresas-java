package br.com.ioasys.imdb.controller;

import br.com.ioasys.imdb.controller.exception.EntityValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author maykon-oliveira
 * @since 6/9/21 4:34 PM
 */
@AllArgsConstructor
@ControllerAdvice
public class ControllerAdviceHandler
    extends org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler {
  private final ModelMapper modelMapper;

  @ExceptionHandler(value = {EntityValidationException.class})
  protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
    final var headers = new HttpHeaders();
    if (ex instanceof EntityValidationException) {
      List<FieldErrorResponse> e = getFieldErrorResponses((EntityValidationException) ex);

      return handleExceptionInternal(ex, e, headers, HttpStatus.BAD_REQUEST, request);
    }
    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request);
  }

  @NotNull
  private List<FieldErrorResponse> getFieldErrorResponses(EntityValidationException ex) {
    final var result = ex.getResult();

    return result.getAllErrors().stream()
        .map(err -> modelMapper.map(err, FieldErrorResponse.class))
        .collect(Collectors.toList());
  }

  @Data
  private static class FieldErrorResponse {
    private String defaultMessage;
    private String field;
    private String rejectedValue;
  }
}
