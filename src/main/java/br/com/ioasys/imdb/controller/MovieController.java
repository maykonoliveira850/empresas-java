package br.com.ioasys.imdb.controller;

import br.com.ioasys.imdb.controller.dto.MovieCreateRequest;
import br.com.ioasys.imdb.controller.exception.EntityValidationException;
import br.com.ioasys.imdb.controller.exception.MovieNotFoundException;
import br.com.ioasys.imdb.entity.Movie;
import br.com.ioasys.imdb.service.MovieService;
import br.com.ioasys.imdb.service.specification.MovieCriteria;
import br.com.ioasys.imdb.utils.ApiResponse;
import br.com.ioasys.imdb.utils.ControllerConstants;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

/**
 * @author maykon-oliveira
 * @since 6/9/21 2:55 PM
 */
@RestController
@AllArgsConstructor
@RequestMapping(ControllerConstants.MOVIE_PATH)
public class MovieController {
  private final MovieService movieService;
  private final ModelMapper mapper;

  @GetMapping
  public Page<Movie> findAll(
      MovieCriteria criteria,
      @SortDefault.SortDefaults({
            @SortDefault(sort = "votes", direction = Direction.DESC),
            @SortDefault(sort = "name", direction = Direction.ASC)
          })
          Pageable pageable) {
    return movieService.findAll(criteria, pageable);
  }

  @GetMapping("{id}")
  public Movie findById(@PathVariable Long id) {
    return movieService.findById(id).orElseThrow(MovieNotFoundException::new);
  }

  @PostMapping
  @ResponseStatus(CREATED)
  public Movie saveMovie(
      @Valid @RequestBody MovieCreateRequest createRequest, BindingResult result) {
    if (result.hasErrors()) {
      throw new EntityValidationException(result);
    }
    final var movie = mapper.map(createRequest, Movie.class);
    return movieService.save(movie);
  }

  @PutMapping("{movieId}/vote/{vote}")
  public ResponseEntity<ApiResponse> voteMovie(
      @PathVariable Long movieId, @PathVariable byte vote) {

    if (!(0 <= vote && vote <= 4)) {
      final var response = ApiResponse.of("Vote should be between 0 and 4");
      return ResponseEntity.badRequest().body(response);
    }

    movieService.voteMovie(movieId, 1L, vote);
    return ResponseEntity.ok().build();
  }
}
