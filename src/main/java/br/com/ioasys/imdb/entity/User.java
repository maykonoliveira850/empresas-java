package br.com.ioasys.imdb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;

/**
 * @author maykon-oliveira
 * @since 6/8/21 7:49 PM
 */
@Data
@Entity
@Table(name = "user")
public class User {
  @Id
  @GeneratedValue(strategy = SEQUENCE)
  private Long id;

  private String name;
  private String email;

  @JsonIgnore
  @Enumerated(STRING)
  private Role role;

  @JsonIgnore private String password;
  @JsonIgnore private boolean excluded;
}
