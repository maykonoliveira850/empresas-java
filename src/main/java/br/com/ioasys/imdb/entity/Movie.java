package br.com.ioasys.imdb.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;

/**
 * @author maykon-oliveira
 * @since 6/8/21 7:43 PM
 */
@Data
@Entity
@Table(name = "movie")
public class Movie {
  @Id
  @GeneratedValue(strategy = SEQUENCE)
  private Long id;

  private String name;
  private int votes;
  private String director;

  @Enumerated(STRING)
  private MovieGender gender;

  @ElementCollection private List<String> authors;

  @Transient private double media;
}
