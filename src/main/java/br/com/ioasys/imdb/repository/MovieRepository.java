package br.com.ioasys.imdb.repository;

import br.com.ioasys.imdb.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:02 PM
 */
public interface MovieRepository
    extends JpaRepository<Movie, Long>, JpaSpecificationExecutor<Movie> {}
