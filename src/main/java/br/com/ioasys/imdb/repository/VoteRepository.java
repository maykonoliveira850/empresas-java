package br.com.ioasys.imdb.repository;

import br.com.ioasys.imdb.entity.Vote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:04 PM
 */
public interface VoteRepository extends CrudRepository<Vote, Long> {
  List<Vote> findAllByMovieId(Long id);

  boolean existsByMovieIdAndUserId(Long movieId, Long userId);
}
