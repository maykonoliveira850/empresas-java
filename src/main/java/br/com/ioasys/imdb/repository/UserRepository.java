package br.com.ioasys.imdb.repository;

import br.com.ioasys.imdb.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:03 PM
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
  boolean existsByEmail(String email);

  Optional<User> findByIdAndExcludedIsFalse(Long id);

  Optional<User> findByEmail(String s);
}
