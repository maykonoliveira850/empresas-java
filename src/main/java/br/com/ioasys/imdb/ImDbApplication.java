package br.com.ioasys.imdb;

import br.com.ioasys.imdb.properties.JwtConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = JwtConfiguration.class)
public class ImDbApplication {

  public static void main(String[] args) {
    SpringApplication.run(ImDbApplication.class, args);
  }
}
