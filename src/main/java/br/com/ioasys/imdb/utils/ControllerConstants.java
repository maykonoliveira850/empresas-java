package br.com.ioasys.imdb.utils;

/**
 * @author maykon-oliveira
 * @since 6/9/21 2:56 PM
 */
public class ControllerConstants {
  public static final String MOVIE_PATH = "/movies";
  public static final String USER_PATH = "/users";

  private ControllerConstants() {}
}
