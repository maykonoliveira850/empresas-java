package br.com.ioasys.imdb.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author maykon-oliveira
 * @since 6/9/21 10:01 PM
 */
@Getter
@Setter
public class ApiResponse {
  private String message;

  private ApiResponse(String message) {
    this.message = message;
  }

  public static ApiResponse of(String message) {
    return new ApiResponse(message);
  }
}
