package br.com.ioasys.imdb.service;

import br.com.ioasys.imdb.controller.exception.MovieNotFoundException;
import br.com.ioasys.imdb.controller.exception.UserAlreadyVotedMovieException;
import br.com.ioasys.imdb.controller.exception.UserNotFoundException;
import br.com.ioasys.imdb.entity.Movie;
import br.com.ioasys.imdb.entity.Vote;
import br.com.ioasys.imdb.repository.MovieRepository;
import br.com.ioasys.imdb.service.specification.CriteriaSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:05 PM
 */
@Service
@AllArgsConstructor
public class MovieService {
  private final MovieRepository movieRepository;
  private final UserService userService;
  private final VoteService voteService;

  public Page<Movie> findAll(CriteriaSpecification<Movie> criteria, Pageable pageable) {
    final Specification<Movie> specification = criteria.toSpecification();
    return movieRepository.findAll(specification, pageable);
  }

  public Optional<Movie> findById(Long id) {
    return movieRepository
        .findById(id)
        .map(
            it -> {
              double media = voteService.getMediaForMovie(it.getId());
              it.setMedia(media);
              return it;
            });
  }

  public Movie save(Movie movie) {
    return movieRepository.save(movie);
  }

  public void voteMovie(Long movieId, Long userId, byte userVote) {
    final var userAlreadyVotedMovie = voteService.existsByMovieIdAndUserId(movieId, userId);
    if (userAlreadyVotedMovie) {
      throw new UserAlreadyVotedMovieException();
    }

    final var movie = movieRepository.findById(movieId).orElseThrow(MovieNotFoundException::new);
    final var user = userService.findById(userId).orElseThrow(UserNotFoundException::new);

    final var vote = Vote.builder().user(user).movie(movie).vote(userVote).build();

    voteService.save(vote);

    movie.setVotes(movie.getVotes() + 1);
    movieRepository.save(movie);
  }
}
