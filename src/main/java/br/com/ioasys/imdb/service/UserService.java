package br.com.ioasys.imdb.service;

import br.com.ioasys.imdb.controller.dto.UserUpdateRequest;
import br.com.ioasys.imdb.controller.exception.EmailAlreadyTakenException;
import br.com.ioasys.imdb.controller.exception.UserNotFoundException;
import br.com.ioasys.imdb.entity.User;
import br.com.ioasys.imdb.repository.UserRepository;
import br.com.ioasys.imdb.security.ApplicationUser;
import br.com.ioasys.imdb.service.specification.CriteriaSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:06 PM
 */
@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  public Page<User> findAll(CriteriaSpecification<User> criteria, Pageable pageable) {
    final var specification = criteria.toSpecification();
    return userRepository.findAll(specification, pageable);
  }

  public User save(User user) {
    if (userRepository.existsByEmail(user.getEmail())) {
      throw new EmailAlreadyTakenException();
    }
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    return userRepository.save(user);
  }

  public void delete(Long id) {
    userRepository
        .findByIdAndExcludedIsFalse(id)
        .ifPresent(
            it -> {
              it.setExcluded(true);
              userRepository.save(it);
            });
  }

  public User update(@Valid UserUpdateRequest user) {
    final var userFound =
        userRepository
            .findByIdAndExcludedIsFalse(user.getId())
            .orElseThrow(UserNotFoundException::new);
    userFound.setName(user.getName());

    return userRepository.save(userFound);
  }

  public Optional<User> findById(Long id) {
    return userRepository.findByIdAndExcludedIsFalse(id);
  }

  @Override
  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    final var user =
        userRepository
            .findByEmail(s)
            .orElseThrow(() -> new UsernameNotFoundException("User " + s + " not found"));

    return new ApplicationUser(user);
  }
}
