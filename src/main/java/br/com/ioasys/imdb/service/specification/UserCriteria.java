package br.com.ioasys.imdb.service.specification;

import br.com.ioasys.imdb.entity.Role;
import br.com.ioasys.imdb.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

/**
 * @author maykon-oliveira
 * @since 6/11/21 12:27 PM
 */
@Getter
@Setter
public class UserCriteria implements CriteriaSpecification<User> {
  private Role role;
  private Boolean excluded;

  @Override
  public Specification<User> toSpecification() {
    Specification<User> specification = Specification.where(null);

    if (Objects.nonNull(role)) {
      specification = specification.and((root, cq, cb) -> cb.equal(root.get("role"), role));
    }
    if (Objects.nonNull(excluded)) {
      specification = specification.and((root, cq, cb) -> cb.equal(root.get("excluded"), excluded));
    }

    return specification;
  }
}
