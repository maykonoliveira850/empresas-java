package br.com.ioasys.imdb.service;

import br.com.ioasys.imdb.entity.Vote;
import br.com.ioasys.imdb.repository.VoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author maykon-oliveira
 * @since 6/8/21 8:06 PM
 */
@Service
@AllArgsConstructor
public class VoteService {
  private final VoteRepository voteRepository;

  public double getMediaForMovie(Long id) {
    return voteRepository.findAllByMovieId(id).stream().mapToInt(Vote::getVote).average().orElse(0);
  }

  public boolean existsByMovieIdAndUserId(Long movieId, Long userId) {
    return voteRepository.existsByMovieIdAndUserId(movieId, userId);
  }

  public Vote save(Vote vote) {
    return voteRepository.save(vote);
  }
}
