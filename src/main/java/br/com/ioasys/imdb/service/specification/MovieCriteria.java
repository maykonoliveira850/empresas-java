package br.com.ioasys.imdb.service.specification;

import br.com.ioasys.imdb.entity.Movie;
import br.com.ioasys.imdb.entity.MovieGender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Objects;

/**
 * @author maykon-oliveira
 * @since 6/9/21 2:42 PM
 */
@Getter
@Setter
public class MovieCriteria implements CriteriaSpecification<Movie> {
  private String director;
  private String name;
  private MovieGender gender;
  private List<String> authors;

  @Override
  public Specification<Movie> toSpecification() {
    Specification<Movie> specification = Specification.where(null);

    if (Objects.nonNull(director) && !director.isBlank()) {
      specification =
          specification.and((root, cq, cb) -> cb.like(root.get("director"), director + "%"));
    }
    if (Objects.nonNull(name) && !name.isBlank()) {
      specification = specification.and((root, cq, cb) -> cb.like(root.get("name"), name + "%"));
    }
    if (Objects.nonNull(gender)) {
      specification = specification.and((root, cq, cb) -> cb.equal(root.get("gender"), gender));
    }
    if (Objects.nonNull(authors) && !authors.isEmpty()) {
      specification =
          specification.and(
              (root, cq, cb) ->
                  cb.and(
                      this.authors.stream()
                          .map(it -> cb.isMember(it, root.get("authors")))
                          .toArray(Predicate[]::new)));
    }
    return specification;
  }
}
