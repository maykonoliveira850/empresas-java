CREATE TABLE user
(
    id       bigint NOT NULL,
    excluded bit    NOT NULL,
    name     varchar(150),
    password varchar(255),
    role     varchar(10),
    PRIMARY KEY (id)
) engine = MyISAM
