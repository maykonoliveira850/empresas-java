CREATE TABLE movie_authors
(
    movie_id bigint NOT NULL,
    authors  varchar(150)
) engine=MyISAM;

ALTER TABLE movie_authors
    ADD CONSTRAINT FKtqbyv5qjcawysmdcq1k0vl3ym foreign key (movie_id) references movie (id)
