CREATE TABLE vote
(
    id       bigint  NOT NULL,
    vote     tinyint NOT NULL,
    movie_id bigint,
    user_id  bigint,
    PRIMARY KEY (id)
) engine = MyISAM;
ALTER TABLE vote
    ADD CONSTRAINT FKhuw0gc5qkscomo1et85v7hnsm FOREIGN KEY (movie_id) REFERENCES movie (id);
ALTER TABLE vote
    ADD CONSTRAINT FKcsaksoe2iepaj8birrmithwve FOREIGN KEY (user_id) REFERENCES user (id);
