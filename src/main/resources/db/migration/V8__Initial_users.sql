SET @currentId = (select next_val
                  from hibernate_sequence for
                  update);

update hibernate_sequence
set next_val= @currentId + 2
where next_val = @currentId;

INSERT INTO user (id, name, password, role, email, excluded)
VALUES (@currentId, 'ADMIN USER', '$2a$10$2EDCP7qln5td80AoLD9woelOFry6H3rIwcwmHrL8CYbAEWhvRL.5W', 'ADMIN',
        'admin@email.com',
        false);

INSERT INTO user (id, name, password, role, email, excluded)
VALUES (@currentId + 1, 'USER', '$2a$10$2EDCP7qln5td80AoLD9woelOFry6H3rIwcwmHrL8CYbAEWhvRL.5W', 'USER',
        'user@email.com',
        false);
