### Como rodar a aplicação

Com o Java 11 instalado: `mvn install clean package -DskipTests`
Tendo o docker instalado: `docker-compose up`

### Documentação da API

Para acessar a documentação da API basta acessar o link
http://localhost:8080/swagger-ui.html com a aplicação rodando.

**Fazer login**

`POST http://localhost:8080/login`

```json
{
  "email": "admin@email.com",
  "password": "123456"
}
```

O token vem no Header Authorization na resposta, deve ser usado nas outras requisições.

### Referencias

https://maykonoliveira850.medium.com/listagem-com-filtragem-din%C3%A2mica-e-pagina%C3%A7%C3%A3o-usando-o-spring-boot-2b99dd1d7050

https://github.com/maykon-oliveira/devdojo-microservices
